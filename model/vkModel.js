var request = require('request')

var config = require('../_config');
var app = {
	id: config.id,
	secretKey: config.secretKey,
	blank: 'https://oauth.vk.com/blank.html',
	responseType: 'token',
	rights: config.rights
}

const authUrl = [
	'https://oauth.vk.com/authorize?',
	'client_id=' + app.id,
	'&redirect_uri=' + app.blank,
	'&response_type=' + app.responseType,
	'&display=page',
	'&v=5.37',
	'&scope=' + app.rights, 
].join('')

var vkModel = function () {
	this.getAuthUrl = function () {
		return Promise.resolve(authUrl)
	};

	this.setToken = function (url) {
		var token = url.slice(url.indexOf('access_token=') + 'access_token='.length, url.indexOf('&',url.indexOf('access_token=')));
		if (!token) {
			throw (new Error ('incorect key, can\'t get token', 'vkModel'));
			return;
		};
		app.token = token;
		return token;
	};

	this.getDialogs = function (options) {
		var options = options || {};
		var count = options.count || 5;
		var uri = ['https://api.vk.com/method/messages.getDialogs?count=',count, '&v=5.37', '&access_token=', app.token].join('');

		if (!app.token) {
			throw new Error('token wasn\'t setted', 'vkModel');
			return;
		}

		var promise = new Promise(function (resolve, reject) {
			request({
				uri: uri,
				method: "GET"
			}, function (err, res ) {
				if (err) {
					reject(err);
					return;
				};
				
				var dialogs = JSON.parse(res.body).response.items;
				resolve(dialogs);
			});
		})
		return promise.catch(console.log)
	};

	this.getUserIdsFromDialogs = function (dialogs) {
		var userIds = dialogs.map(function (dialog) {
			return dialog.message.user_id
		});
		var promise = Promise.resolve(userIds);

		return promise.catch(console.log);
	};

	this.getUsersByIds = function (userIds, options) {
		var userIds = userIds.join(',');
		var uri = ['https://api.vk.com/method/users.get?user_ids=',userIds,'&v=5.37'].join('');
		var promise = new Promise(function (resolve, reject) {
			request({
				uri: uri,
				method: "GET"
			}, function (err, res ) {
				if (err) {
					reject(err);
					return;
				};
				var users = JSON.parse(res.body).response;
				resolve(users);
			});
		})
		return promise.catch(console.log);
	};

	this.getNames = function (users) {
		var names = users.map(function (user) {
			return [user.first_name, user.last_name].join(' ');
		});
		var promise = Promise.resolve(names);
		return promise.catch(console.log);
	};

	this.getHistoryByUserId = function (userId, options) {
			var options = options || {};
			var count = options.count || 5;
			var uri = ['https://api.vk.com/method/messages.getHistory?user_id=', userId,'&count=',count,'&v=5.37&access_token=',app.token].join('');
			var promise = new Promise(function (resolve, reject) {
				request({
					uri: "https://api.vk.com/method/messages.getHistory?user_id="+ users[param].id +"&count=5&v=5.37&access_token=" + app.token,
					method: "GET"
				}, function (err, res ) {
					if (err) {
						reject(err)
						return
					}
					var dialog = JSON.parse(res.body).response;
					resolve(dialog);
				});
			});
			return promise.catch(console.log);
	}
}
module.exports = new vkModel;