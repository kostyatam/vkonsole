'use strict'
var open = require('open');
var readline = require('readline');
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});
var co = require('co');
var EventEmitter = require('events').EventEmitter;
var vk = require('./model/vkModel');
var util = require('util');
var clear = require('cli-clear');

var View = function () {
	this.dialogs = '';
	this.message = '';
	this.redraw = function () {
		clear();
		console.log(this.dialogs + this.message)
	};
	this.on('redraw', function (data) {
		this.dialogs = data.dialogs || this.message;
		this.message = data.message || this.message;
		this.redraw();
	});
	this.initialize = function () {
		var setToken = function() {
			var promise = new Promise(function(resolve, reject) {
				rl.question('please insert url sir\n', function(key) {
					resolve(key);
				})
			})
			return promise;
		};
		function* getDialogs () {
			var dialogs = yield vk.getDialogs();
			var userIds = yield vk.getUserIdsFromDialogs(dialogs);
			var users = yield vk.getUsersByIds(userIds);
			var names = yield vk.getNames(users);

			var displayDialog = dialogs.map(function(dialog, index) {
				return ['[', index, '] ', names[index], ' : ', dialog.message.body].join('')
			}).join('\n');

			view.emit('redraw', {
				dialogs: displayDialog
			})
		}
		co(function*() {
			var authUrl = yield vk.getAuthUrl();
			open(authUrl, function(err) {});
			var key = yield setToken();
			vk.setToken(key);
			yield co(getDialogs).catch(console.log);
			this.timerId = setInterval(function () {
					co(getDialogs).catch(console.log)
				}, 3000)
		}).catch(console.log);
	};
	this.clear = function () {
		clearTimeout(this.timerId);
		this.removeAllListeners();
	}
};

util.inherits(View, EventEmitter);

var view = new View;
view.initialize();
