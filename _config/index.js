/*
## copy to config.local.js
var config = {
	id: yourId,
	secretKey: yourSecretkey,
	rights: yourRights
}

module.export = config
*/

var config = require('./config.local.js');
module.exports = config;